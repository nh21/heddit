{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE TypeApplications #-}

-- |
-- Module      : Network.Reddit.Message
-- Copyright   : (c) 2021 Rory Tyler Hayford
-- License     : BSD-3-Clause
-- Maintainer  : rory.hayford@protonmail.com
-- Stability   : experimental
-- Portability : GHC
--
-- Actions for messaging. These can be both comment replies as well as private
-- messages
--
module Network.Reddit.Message
    (  -- * Actions
      getInbox
    , getUnread
    , getSent
    , getMessage
    , markRead
    , markAllRead
    , collapseMessages
    , uncollapseMessages
    , sendMessage
    , deleteMessage
    , replyToMessage
    , reportMessage
      -- * Types
    , module M
    ) where

import           Control.Monad.Catch

import           Data.Bool
import           Data.Foldable
import           Data.Generics.Wrapped
import           Data.List.Split
import           Data.Sequence                ( Seq(Empty, (:<|)) )
import           Data.Text                    ( Text )

import           Lens.Micro

import           Network.Reddit.Internal
import           Network.Reddit.Types
import           Network.Reddit.Types.Item
import           Network.Reddit.Types.Message
import           Network.Reddit.Types.Message as M
                 ( Message(Message)
                 , MessageID
                 , MessageOpts(MessageOpts)
                 , NewMessage(NewMessage)
                 , PrivateMessageID(PrivateMessageID)
                 )
import           Network.Reddit.Utils

import           Web.FormUrlEncoded
import           Web.HttpApiData

-- | Get the 'Message' inbox for the currently authenticated user
getInbox :: MonadReddit m
         => Paginator MessageID Message
         -> m (Listing MessageID Message)
getInbox = msgs "inbox"

-- | Get the unread 'Message's of the currently authenticated user
getUnread :: MonadReddit m
          => Paginator MessageID Message
          -> m (Listing MessageID Message)
getUnread = msgs "unread"

-- | Get the 'Message's sent by the currently authenticated user
getSent :: MonadReddit m
        => Paginator MessageID Message
        -> m (Listing MessageID Message)
getSent = msgs "sent"

-- | Get the message corresponding to a 'MessageID'. An 'InvalidResponse'
-- exception will be thrown if the message does not exist
getMessage :: MonadReddit m => MessageID -> m Message
getMessage mid = do
    Listing { children } <- runAction @(Listing MessageID Message) r
    case children of
        Empty     -> throwM $ InvalidResponse "No such message found"
        msg :<| _ -> pure msg
  where
    r = defaultAPIAction
        { pathSegments = [ "message", "messages", toQueryParam mid ] }

-- | Mark a 'Message' as read
markRead :: MonadReddit m => MessageID -> m ()
markRead mid =
    runAction_ defaultAPIAction
               { pathSegments = [ "api", "read_message" ]
               , method       = POST
               , requestData  = mkTextFormData [ ("id", fullname mid) ]
               }

-- | Mark /all/ messages as read for the authenticated user
--
-- __Note__: This simply queues the messages for marking as read; it make take
-- some time to complete the request after it has been received by Reddit
markAllRead :: MonadReddit m => m ()
markAllRead = runAction_ defaultAPIAction
                         { pathSegments = [ "api", "read_all_messages" ]
                         , method       = POST
                         }

-- | Mark each of a container of 'MessageID' as collapsed
collapseMessages :: (MonadReddit m, Foldable t) => t MessageID -> m ()
collapseMessages = collapse True

-- | Mark each of a container of 'MessageID' as collapsed
uncollapseMessages :: (MonadReddit m, Foldable t) => t MessageID -> m ()
uncollapseMessages = collapse False

-- | Mark each of a container of 'MessageID' as collapsed. An arbitrary number
-- of IDs can be included; requests will be sent in batches of 25
collapse :: (MonadReddit m, Foldable t) => Bool -> t MessageID -> m ()
collapse b mids = for_ chunked $ \ch ->
    runAction_ defaultAPIAction
               { pathSegments =
                     [ "api", bool "uncollapse" "collapse" b <> "_message" ]
               , method       = POST
               , requestData  = mkTextFormData [ ("id", fullname ch) ]
               }
  where
    -- it appears the limit here may be 25, rather than 100 (i.e. @apiRequestLimit@)
    chunked = chunksOf 25 $ toList mids

-- | Send a 'NewMessage'  to another user
sendMessage :: MonadReddit m => NewMessage -> m ()
sendMessage newMsg =
    runAction_ defaultAPIAction
               { pathSegments = [ "api", "compose" ]
               , method       = POST
               , requestData  = WithForm $ toForm newMsg
               }

-- | Delete a single message, given its ID. This action will fail silently
-- if given invalid message IDs
deleteMessage :: MonadReddit m => MessageID -> m ()
deleteMessage mid =
    runAction_ defaultAPIAction
               { pathSegments = [ "api", "del_msg" ]
               , method       = POST
               , requestData  = mkTextFormData [ ("id", fullname mid) ]
               }

-- | Reply to a 'Message', returning the newly created 'Message'
replyToMessage :: MonadReddit m => MessageID -> Body -> m Message
replyToMessage mid txt = runAction @PostedMessage r <&> wrappedTo
  where
    r = defaultAPIAction
        { pathSegments = [ "api", "comment" ]
        , method       = POST
        , requestData  = WithForm
              $ mkTextForm [ ("thing_id", fullname mid)
                           , ("text", txt)
                           , ("api_type", "json")
                           ]
        }

msgs :: MonadReddit m
     => Text
     -> Paginator MessageID Message
     -> m (Listing MessageID Message)
msgs path paginator = runAction defaultAPIAction
                                { pathSegments = [ "message", path ]
                                , requestData  = paginatorToFormData paginator
                                }

-- | Report a message, bringing it to the attention of the Reddit admins
reportMessage :: MonadReddit m => Report -> MessageID -> m ()
reportMessage r mid =
    runAction_ defaultAPIAction
               { pathSegments = [ "api", "report" ]
               , method       = POST
               , requestData  = mkTextFormData [ ("id", toQueryParam mid)
                                               , ("reason", toQueryParam r)
                                               ]
               }
