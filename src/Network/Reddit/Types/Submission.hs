-- |
-- Module      : Network.Reddit.Types.Submission
-- Copyright   : (c) 2021 Rory Tyler Hayford
-- License     : BSD-3-Clause
-- Maintainer  : rory.hayford@protonmail.com
-- Stability   : experimental
-- Portability : GHC
--
module Network.Reddit.Types.Submission
    {-# DEPRECATED "Use Network.Reddit.Types.Item directly" #-}
    ( Submission(..)
    , SubmissionID(SubmissionID)
    , SubmissionContent(..)
    , submissionP
    , PollData(..)
    , PollOption(..)
    , PollOptionID
      -- * Collections
    , Collection(..)
    , CollectionLayout(..)
    , CollectionID
    , NewCollection(..)
      -- * Creating submissions
    , SubmissionOptions(..)
    , mkSubmissionOptions
    , NewSubmission(..)
    , S3UploadLease(..)
    , UploadType(..)
    , UploadResult(..)
    , CrosspostOptions(..)
    , mkCrosspostOptions
    , PostedCrosspost
    , Poll(..)
    , PollSubmission(PollSubmission)
    , mkPoll
    , GalleryImage(..)
    , mkGalleryImage
    , galleryImageToUpload
    , GallerySubmission(GallerySubmission)
    , InlineMedia(..)
    , InlineMediaType(..)
    , InlineMediaUpload(..)
    , inlineMediaToUpload
    , writeInlineMedia
    , Fancypants
    , PostedSubmission
      -- * Search
    , Search(..)
    , SearchSort(..)
    , SearchCategory
    , mkSearchCategory
    , SearchOpts(..)
    , ResultID(ResultID)
    , SearchSyntax(..)
    , mkSearch
    ) where

import           Network.Reddit.Types.Item
