{ compiler ? "ghc8107", sources ? import ./nix/sources.nix }:

let
  pkgs = import sources.nixpkgs {};

  gitignore = pkgs.nix-gitignore.gitignoreSourcePure [ ./.gitignore ];

  hpkgs = pkgs.haskell.packages.${compiler}.override {
    overrides = self: super: {
      "heddit" = self.callCabal2nix "heddit" (gitignore ./.) {};
    };
  };

  shell = hpkgs.shellFor {
    packages = p: [ p."heddit" ];
    configureFlags = [ "-f io-tests" ];
    buildInputs = with pkgs.haskellPackages; [
      (
        pkgs.haskell-language-server.override {
          supportedGhcVersions = [ "8107" ];
        }
      )
      pkgs.niv
      cabal-install
      hlint
      floskell
      implicit-hie
      cabal-fmt
    ];
  };

in
{
  inherit shell;
  "heddit" = hpkgs."heddit";
}
